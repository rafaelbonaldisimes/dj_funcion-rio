Sistema de Crud - funcionário 
PHP/Html,js,css,ajax,jQuery

Passo 1 – O banco de dados está disponível em duas formas. O arquivo “bd.sql” possui todas as querys necessárias. E o arquivo “funcionarios_dj.sql” basta ser importado para seu servidor.

Passo 2 – O arquivo “config.php” é o arquivo que faz conexão com o banco em questão.
Basta alterar os seguintes campos:
$db_banco ="Nome_do_banco_de_dados";
$db_user = "Usuario_do_servidor";
$db_pass = "Senha_do_servidor";
$host = 'Servidor';

Exemplo:
$db_banco ="funcionarios_dj";
$db_user = "Adm";
$db_pass = "Adm";
$host = 'localhost';

Passo 3 - Login e senha para acesso:

Login:pedro@dj.emp.br,Senha:12345;
Login:joao@dj.emp.br,Senha:12345;
Login:flavio@dj.emp.br,Senha:12345;
Login:maria@dj.emp.br,Senha:12345;
Login:amanda@dj.emp.br,Senha:12345;
Login:manoel@dj.emp.br,Senha:12345;
Login:laura@dj.emp.br,Senha:12345;
Login:debora@dj.emp.br,Senha:12345;
Login:bruno@dj.emp.br,Senha:12345;
Login:rodrigo@dj.emp.br,Senha:12345;