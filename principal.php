<!DOCTYPE HTML>
<?php include("verifica.php"); ?>
<html>
	<head>
		<link rel="icon" type="img/png" href="img/logo.png" />
		<title>Menu</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="css/main.css" />
		<script type="text/javascript" src="js/scriptIndex.js"></script>  
	</head>
	<body>
		<form>
		<div id="page-wrapper">
			<div id="clock"></div>
			<div id="AtualizarNotificacao"></div>
			<div id="AtualizarMensagem"></div>
			<nav id="nav">
				<ul>
					<li class="current"><a href="funcionario_cadastro.php">Cadastrar Funcionário</a></li>
					<li><a href="sair.php">Sair </a></li>
				</ul>
			</nav>
		</div>
			
		<div id="page-wrapper">
			<div id="clock"></div>
				<header id="header">
					<div class="logo container">
						<div>
							<div>
								Seja bem vindo <?php echo $_SESSION['nome']; ?>
							</div>
						</div>
					</div>
				</header>
			</div>
		</div>

			<script src="js/jquery.min.js"></script>
			<script src="js/jquery.dropotron.min.js"></script>
			<script src="js/skel.min.js"></script>
			<script src="js/skel-viewport.min.js"></script>
			<script src="js/util.js"></script>
			<script src="js/main.js"></script>
		</form>
	</body>
</html>