<?php 
	$id = $_GET['codFuncionario'];
		
	include("config.php");
	$res = mysql_query("SELECT codFuncionario, nome, email, b.descricao as setor, c.descricao as cargo , foto, a.codSetor,a.codCargo,senha FROM tb_funcionario as a
		inner join tb_setor as b on a.codSetor = b.codSetor
		inner join tb_cargo as c on a.codCargo = c.codCargo
		where codFuncionario = $id");
		$total = mysql_num_rows($res);

		for($i=0; $i<$total; $i++){
			$dados = mysql_fetch_row($res);
			$codFuncionario = $dados[0];
			$nome = $dados[1];
			$email = $dados[2];
			$setor = $dados[3];
			$cargo = $dados[4];
			$foto = $dados[5];
			$codSetor = $dados[6];
			$codCargo = $dados[7];
			$senha = $dados[8];
	}
?>
<html>
	<script type="text/javascript" src="js/jquery.mascaras.js"></script>
	<body>
		<form class="cadastro" id="form1" name="form1" enctype="multipart/form-data" method="post" action="funcionario_alteracao_php.php">
			<div id="page-wrapper">	
				<input type="hidden" name="IdFuncionario" id="IdFuncionario" value="<?php echo $id;?>">
				<div class="row" style="margin-top:10px">
					<div class="6u 12u(mobile)">
					<label style="text-align:left;">
						Nome *
					</label>
					<input type="text" name="TxtNomeAlt" id="TxtNomeAlt" onkeypress='return Letras(event)' value="<?php echo $nome;?>" required/>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						E-mail *
					</label>
					<input type="text" name="TxtEmailAlt" id="TxtEmailAlt" onblur="validacaoEmail(this)" maxlength="60" size='65' value="<?php echo $email;?>" required>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Senha *
					</label>
					<input type="text" name="TxtSenhaAlt" id="TxtSenhaAlt" maxlength="20" value="<?php $senha = base64_decode($senha); echo $senha;?>" required>
				</div>
				
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Setor *
					</label>
					<select name="slSetorAlt" id="slSetorAlt" required>
						<option value="<?php echo $codSetor;?>"><?php echo $setor;?></option>
						<?php
							include("config.php");
							$res = mysql_query("SELECT codSetor, descricao FROM tb_setor where codSetor <> '$codSetor' order by descricao");
							$total = mysql_num_rows($res);

							for($i=0; $i<$total; $i++){
								$dados = mysql_fetch_row($res);
								$codSetor = $dados[0];
								$setor = $dados[1];
								
								echo '<option value="'.$codSetor.'">'.$setor.'</option>';
							}
						?>
					</select>
				</div>
				
				
				
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Cargo *
					</label>
					<select name="slCargoAlt" id="slCargoAlt" required>
						<option value="<?php echo $codCargo;?>"><?php echo $cargo;?></option>
						<?php
							include("config.php");
							$res = mysql_query("SELECT codCargo, descricao FROM tb_cargo where codCargo <> '$codCargo' order by descricao");
							$total = mysql_num_rows($res);

							for($i=0; $i<$total; $i++){
								$dados = mysql_fetch_row($res);
								$codCargo = $dados[0];
								$cargo = $dados[1];
								
								echo '<option value="'.$codCargo.'">'.$cargo.'</option>';
							}
						?>
					</select>
				</div>
				
				<div class="6u 12u(mobile)">
					<label style="text-align:left;">
						Foto
					</label>
					<div>
					<?php 
						if($foto != "") 
						{
							echo '<b>Foto Atual:</b> <img height="80" width="80" src="fotoFuncionario\\'.$foto.'">';
						}
						else
						{
							echo "<b>Funcionário não possui foto.</b>";
						}
					?>
					</div>
					<div>
						<input type="file" name="flFotoAlt" id="flFotoAlt" class="form-control"/>
					</div>
				</div>
					
					
					
					
					<div class="12u 12u(mobile)" style="margin-top:10px;text-align:left;"><link href="funcionario_alteracao_php.php"><input type="submit" class="btAmarelo" value="Alterar"></link></div>
				</div>
			</div>
		</form>
	</body>
</html>