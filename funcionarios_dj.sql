-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02-Maio-2016 às 23:41
-- Versão do servidor: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `funcionarios_dj`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_cargo`
--

CREATE TABLE IF NOT EXISTS `tb_cargo` (
  `codCargo` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`codCargo`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `tb_cargo`
--

INSERT INTO `tb_cargo` (`codCargo`, `descricao`) VALUES
(1, 'Gerente'),
(2, 'Programador'),
(3, 'Estagiário'),
(4, 'Secretária'),
(5, 'Contador'),
(6, 'Psicóloga'),
(7, 'CEO'),
(8, 'CFO');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionario`
--

CREATE TABLE IF NOT EXISTS `tb_funcionario` (
  `codFuncionario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `senha` varchar(20) NOT NULL,
  `codSetor` int(11) NOT NULL,
  `codCargo` int(11) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`codFuncionario`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Extraindo dados da tabela `tb_funcionario`
--

INSERT INTO `tb_funcionario` (`codFuncionario`, `nome`, `email`, `senha`, `codSetor`, `codCargo`, `foto`) VALUES
(1, 'Pedro', 'pedro@dj.emp.br', 'MTIzNDU=', 1, 1, NULL),
(2, 'JoÃ£o', 'joao@dj.emp.br', 'MTIzNDU=', 1, 2, NULL),
(3, 'Flavio', 'flavio@dj.emp.br', 'MTIzNDU=', 1, 3, NULL),
(4, 'Maria', 'maria@dj.emp.br', 'MTIzNDU=', 2, 4, NULL),
(5, 'Amanda', 'amanda@dj.emp.br', 'MTIzNDU=', 2, 3, NULL),
(6, 'Manoel', 'manoel@dj.emp.br', 'MTIzNDU=', 3, 5, NULL),
(7, 'Laura', 'laura@dj.emp.br', 'MTIzNDU=', 4, 6, NULL),
(8, 'Debora', 'debora@dj.emp.br', 'MTIzNDU=', 4, 1, NULL),
(9, 'Bruno', 'bruno@dj.emp.br', 'MTIzNDU=', 5, 7, NULL),
(10, 'Rodrigo', 'rodrigo@dj.emp.br', 'MTIzNDU=', 5, 8, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_setor`
--

CREATE TABLE IF NOT EXISTS `tb_setor` (
  `codSetor` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`codSetor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `tb_setor`
--

INSERT INTO `tb_setor` (`codSetor`, `descricao`) VALUES
(1, 'TI'),
(2, 'Administrativo'),
(3, 'Financeiro'),
(4, 'RH'),
(5, 'Diretoria');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
