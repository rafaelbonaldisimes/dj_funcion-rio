create database funcionarios_dj;

CREATE TABLE tb_setor(
codSetor int(11) NOT NULL auto_increment,
descricao varchar(255) NOT NULL ,
PRIMARY KEY (codSetor)
)ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
INSERT INTO tb_setor(descricao) VALUES ('TI');
INSERT INTO tb_setor(descricao) VALUES ('Administrativo');
INSERT INTO tb_setor(descricao) VALUES ('Financeiro');
INSERT INTO tb_setor(descricao) VALUES ('RH');
INSERT INTO tb_setor(descricao) VALUES ('Diretoria');

CREATE TABLE tb_cargo(
codCargo int(11) NOT NULL auto_increment,
descricao varchar(255) NOT NULL ,
PRIMARY KEY (codCargo)
)ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
INSERT INTO tb_cargo(descricao) VALUES ('Gerente');
INSERT INTO tb_cargo(descricao) VALUES ('Programador');
INSERT INTO tb_cargo(descricao) VALUES ('Estagiário');
INSERT INTO tb_cargo(descricao) VALUES ('Secretária');
INSERT INTO tb_cargo(descricao) VALUES ('Contador');
INSERT INTO tb_cargo(descricao) VALUES ('Psicóloga');
INSERT INTO tb_cargo(descricao) VALUES ('CEO');
INSERT INTO tb_cargo(descricao) VALUES ('CFO');

CREATE TABLE tb_funcionario(
codFuncionario int(11) NOT NULL auto_increment,
nome varchar(255) NOT NULL ,
email varchar(255) NOT NULL ,
senha varchar(20) NOT NULL ,
codSetor int(11) NOT NULL,
codCargo int(11) NOT NULL,
foto varchar(255) NULL ,
PRIMARY KEY (codFuncionario)
)ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;
INSERT INTO tb_funcionario (nome, email, senha, codSetor, codCargo, foto) VALUES
('Pedro', 'pedro@dj.emp.br', 'MTIzNDU=', 1, 1, NULL),
('JoÃ£o', 'joao@dj.emp.br', 'MTIzNDU=', 1, 2, NULL),
('Flavio', 'flavio@dj.emp.br', 'MTIzNDU=', 1, 3, NULL),
('Maria', 'maria@dj.emp.br', 'MTIzNDU=', 2, 4, NULL),
('Amanda', 'amanda@dj.emp.br', 'MTIzNDU=', 2, 3, NULL),
('Manoel', 'manoel@dj.emp.br', 'MTIzNDU=', 3, 5, NULL),
('Laura', 'laura@dj.emp.br', 'MTIzNDU=', 4, 6, NULL),
('Debora', 'debora@dj.emp.br', 'MTIzNDU=', 4, 1, NULL),
('Bruno', 'bruno@dj.emp.br', 'MTIzNDU=', 5, 7, NULL),
('Rodrigo', 'rodrigo@dj.emp.br', 'MTIzNDU=', 5, 8, NULL);
