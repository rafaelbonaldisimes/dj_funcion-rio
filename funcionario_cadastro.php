<!DOCTYPE HTML>
<?php include("verifica.php");?>
<html>
	<head>
		<title>Cadastro Funcionário</title>
		<link rel="icon" type="img/png" href="img/logo.png" />
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="css/main.css" />
		<script type="text/javascript" src="js/scriptIndex.js"></script>
		<link rel="stylesheet" href="css/style.css" />
        <script src="js/prefixfree.min.js"></script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>  
		<script type="text/javascript">
        window.onload = function(){
		  atualizarTodos();
		}
        </script> 
	</head>
	<body>
	
	<div id="page-wrapper">
		<div id="clock"></div>
		<div id="AtualizarNotificacao"></div>
		<div id="AtualizarMensagem"></div>
		<nav id="nav">
			<ul>
				<li class="current"><a href="principal.php">Voltar</a></li>
			</ul>
		</nav>
	</div>
	
	<header id="header">
		<div class="logo container">
			<div>
				<div>
					Cadastro Funcionário
				</div>
			</div>
		</div>
	</header>
	
	<form class="cadastro" id="form1" name="form1" enctype="multipart/form-data" method="post" action="funcionario_cadastro_php.php">
  
	  <input class="ocultarRadio" id="tab1" type="radio" name="tabs" onclick="OcultarAbaEditar()" checked>
	  <label for="tab1">Todos</label>
		
	  <input class="ocultarRadio" id="tab2" type="radio" name="tabs" onclick="OcultarAbaEditar()">
	  <label for="tab2">Cadastro</label>
	  
	   <input class="ocultarRadio" id="tab3" type="radio" name="tabs" readonly>
	  <label for="tab3" id="endTimeLabel" style="display:none;">Alterar</label>
		
	  <section id="content1">
		<div id="page-wrapper">	
			<div class="row" style="margin-top:10px; margin-bottom:10px;">
				<div class="4u 12u(mobile)">
					<label style="text-align:left;">
						Nome
					</label>
					<input type="text" name="TxtNome" id="TxtNome" onkeypress='return Letras(event)'/>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Setor
					</label>
					<select name="slSetor" id="slSetor">
						<option value="">&nbsp;</option>
						<?php
						include("config.php");
						$res = mysql_query("SELECT codSetor, descricao FROM tb_setor order by descricao");
						$total = mysql_num_rows($res);

						for($i=0; $i<$total; $i++){
							$dados = mysql_fetch_row($res);
							$codSetor = $dados[0];
							$descricao = $dados[1];
							
							echo '<option value="'.$codSetor.'">'.$descricao.'</option>';
						}
						?>
					</select>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Cargo
					</label>
					<select name="slCargo" id="slCargo">
						<option value="">&nbsp;</option>
						<?php
						include("config.php");
						$res = mysql_query("SELECT codCargo, descricao FROM tb_cargo order by descricao");
						$total = mysql_num_rows($res);

						for($i=0; $i<$total; $i++){
							$dados = mysql_fetch_row($res);
							$codCargo = $dados[0];
							$descricao = $dados[1];
							
							echo '<option value="'.$codCargo.'">'.$descricao.'</option>';
						}
						?>
					</select>
				</div>
				
				<div class="2u 12u(mobile)">
					<label style="text-align:left;">
						Filtrar
					</label>
					<div>
						<input type="submit" value="Filtrar" onclick="atualizarTodos();">
					</div>
				</div>
				
			</div>
		</div>
		
		<div id="TotalFuncionario"> 
		
		</div>
		
	  </section>
		
	  <section id="content2">
		<div id="page-wrapper">	
			<div class="row" style="margin-top:10px">
				<div class="6u 12u(mobile)">
					<label style="text-align:left;">
						Nome *
					</label>
					<input type="text" name="TxtNome" id="TxtNome" onkeypress='return Letras(event)' required/>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						E-mail *
					</label>
					<input type="text" name="TxtEmail" id="TxtEmail" onblur="validacaoEmail(this)" required>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Senha *
					</label>
					<input type="text" name="TxtSenha" id="TxtSenha" maxlength="20" required>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Setor *
					</label>
					<select name="slSetor" id="slSetor" required>
						<option value="">&nbsp;</option>
						<?php
						include("config.php");
						$res = mysql_query("SELECT codSetor, descricao FROM tb_setor order by descricao");
						$total = mysql_num_rows($res);

						for($i=0; $i<$total; $i++){
							$dados = mysql_fetch_row($res);
							$codSetor = $dados[0];
							$descricao = $dados[1];
							
							echo '<option value="'.$codSetor.'">'.$descricao.'</option>';
						}
						?>
					</select>
				</div>
				<div class="3u 12u(mobile)">
					<label style="text-align:left;">
						Cargo *
					</label>
					<select name="slCargo" id="slCargo" required>
						<option value="">&nbsp;</option>
						<?php
						include("config.php");
						$res = mysql_query("SELECT codCargo, descricao FROM tb_cargo order by descricao");
						$total = mysql_num_rows($res);

						for($i=0; $i<$total; $i++){
							$dados = mysql_fetch_row($res);
							$codCargo = $dados[0];
							$descricao = $dados[1];
							
							echo '<option value="'.$codCargo.'">'.$descricao.'</option>';
						}
						?>
					</select>
				</div>
				<div class="6u 12u(mobile)">
					<label style="text-align:left;">
						Foto
					</label>
					<div>
						<input type="file" name="flFoto" id="flFoto" class="form-control"/>
					</div>
				</div>
				
				<div class="12u 12u(mobile)" style="margin-top:10px;text-align:left;">
					<input type="submit" value="Salvar">
				</div>
			</div>
		</div>
	  </section>
	  
	  <section id="content3">
		
	  </section>
	  
		<!-- Scripts -->
			<script src="js/jquery.min.js"></script>
			<script src="js/jquery.dropotron.min.js"></script>
			<script src="js/skel.min.js"></script>
			<script src="js/skel-viewport.min.js"></script>
			<script src="js/util.js"></script>
			<script src="js/main.js"></script>
			<script type="text/javascript" src="js/jquery-1.10.2.min.js"></script>
			<script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
			<script type="text/javascript" src="js/jquery.validate.min.js"></script>
			<script type="text/javascript" src="js/jquery.zebra-datepicker.js"></script>
	</form>
	</body>
</html>

